#!/usr/bin/env python

"""
    pywiki.py - commandline wiki search and article viewer
"""

# file     : pywiki.py
# purpose  : search and view wikipedia articles on a commandline
#
# author   : harald van der laan
# date     : 2017/02/02
# version  : v1.0.0

from __future__ import print_function
import sys
import wikipedia

def search_wiki(query):
    """ function to search wikipedia.org """
    result = wikipedia.search(query)

    return result

def view_wiki(article, summary=None):
    """ function to view wikipedia.org articles """
    if not summary:
        result = wikipedia.page(article)
    else:
        result = wikipedia.summary(article)

    return result

def main(action, value):
    """ main function """
    if action == "search":
        for results in search_wiki(value):
            print('{}' .format(results.encode('utf-8')))
    elif action == "summary":
        print(view_wiki(value, summary=True).encode('utf-8'))
    elif action == "page":
        print(view_wiki(value).content.encode('utf-8'))
    else:
        sys.stderr.write('[-] action type can only be: search, summary or page.\n')
        sys.exit(1)

if __name__ == "__main__":
    if len(sys.argv) < 3 or len(sys.argv) > 4:
        sys.stderr.write('[-] usage: {} <search|summary|page> \'<article>\' [nl|en]\n'
                         .format(sys.argv[0]))
        sys.exit(1)

    try:
        if sys.argv[3]:
            if sys.argv[3] == 'nl' or sys.argv[3] == 'en':
                # optional argument language is dutch or english
                wikipedia.set_lang(sys.argv[3])
            else:
                sys.stderr.write('[-] language can only be nl or en.\n')
                sys.exit(1)
    except IndexError:
        # fallack to default dutch language
        wikipedia.set_lang('nl')

    main(sys.argv[1], sys.argv[2])
    sys.exit(0)
