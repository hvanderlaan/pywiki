# Pywiki
pywiki is a small script for searching and viewing [wikipedia](https://wikipedia.org) articles on a linux/unix commandline. 

## Installation
```bash
sudo -H pip install -r requirements.txt --upgrade
```

## Usage
```bash
./pywiki.py <search|summary|page> '<article>' [nl|en]
./pywiki.py search hydrogen en
./pywiki.py summary hydorgen en
./pywiki.py page hydrogen en

# default language is dutch
./pywiki.py search waterstof
```

## Todo
- [x] Add multi language support (dutch and english)
- [ ] Filter out disambiguation pages and show related articles
- [x] Add gitlab-ci support

## License
pywiki is licensed under the GPLv3:
```
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

For the full license, see the LICENSE file.
```
